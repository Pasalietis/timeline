class Scores {
	actions = { 'HOME': [], 'AWAY': [] };
	scoreWidth = 5;
	spaceBetweenScores = 2;

	constructor(seconds) {
		this.app = $('#app');
		this.seconds = seconds;
		this.draw();
		this.calculateTimePerPixel();

		let timer;

		$(window).resize(() => {
			this.calculateTimePerPixel();

			clearTimeout(timer);
			timer = setTimeout(this.drawScores(), 500);
		});
	};

	addAction(seconds, team) {
		if (this.actions[team] === undefined) {
			throw Error('Third team??');
		}

		this.actions[team].push(seconds);
		this.actions[team].sort((a, b) => a - b);

		this.drawScores();
	};

	draw() {
		const el = $('<table id="timeline">');

		for (let i = 0; i < 10; i++) {
			el.append('<td>');
		}

		this.timeline = el;
		this.app.append(el);
	}

	drawScores() {
		this.app.find('.score').remove();
		let i = 0;

		$.each(this.actions, (team, actions) => {
			let color;
			let drawed = [];
			i++;

			if (i === 1) {
				color = 'blue';
			} else if (i === 2) {
				color = 'red';
			} else {
				throw Error('wtf?');
			}

			$.each(actions, (key, time) => {
				let data = {
					time,
					position: this.timeToPosition(time, this.scoreWidth),
					width: this.scoreWidth,
					score: 1,
				};

				for (let i = drawed.length - 1; i >= 0; i--) {
					let prevData = drawed[i];

					if (prevData.position + prevData.width + this.spaceBetweenScores < data.position) {
						break;
					}

					prevData.score += data.score;

					if (data.el) {
						data.el.remove();
					}

					data = prevData;
					this.drawScore(color, data);
					drawed.splice(i, 1);
				}

				this.drawScore(color, data);
				drawed.push(data);
			});
		});
	}

	drawScore(color, data) {
		if (data.el !== undefined) {
			let html = data.el.html();

			if (html === '' && data.score === 1) {
				return true;
			} else if (html === data.score) {
				return true;
			}

			data.el.remove();
		}

		const el = $('<div class="score">');
		el.addClass(color);

		if (data.score > 1) {
			el.addClass('block');
			el.html(data.score);
		}

		this.app.append(el);

		data.el = el;
		data.width = el.outerWidth();
		data.position = this.timeToPosition(data.time, data.width);

		el.css('left', data.position);
	}

	calculateTimePerPixel() {
		this.timePerPixel = this.app.width() / this.seconds;
	}

	timeToPosition(time, width) {
		let pixel = this.timePerPixel * time - width / 2;

		if (pixel < 0) {
			pixel = 0;
		} else if (pixel + width > this.app.width()) {
			pixel = this.app.width() - width;
		}
		return pixel;
	}
}

export default Scores;
