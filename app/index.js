import './css/style.scss';
import Scores from './js/scores';

let scores;

const lib = {
	init(seconds) {
		scores = new Scores(seconds);
	},
	addAction(seconds, team) {
		if(scores){
			scores.addAction(seconds, team);
		}
	},
};

module.exports = lib;
