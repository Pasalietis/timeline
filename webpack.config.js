const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
	entry: {
		app: './app',
	},
	output: {
		path: path.resolve(__dirname, 'build'),
		filename: '[name].js',
		chunkFilename: '[id].js',
		libraryTarget: 'this',
	},
	resolve: {
		alias: {
			jquery: "jquery/src/jquery"
		}
	},
	module: {
		loaders: [
			{
				test: /\.css$/,
				include: [path.resolve(__dirname, 'app')],
				loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader' }),
			},
			{
				test: /\.(js|jsx)$/,
				loader: 'babel-loader',
				include: [path.resolve(__dirname, 'app')],
				query: {
					presets: ['es2015', 'stage-0'],
					plugins: ['transform-runtime'],
				},
			},
			{
				test: /\.scss$/,
				loaders: ['style-loader', 'css-loader', 'sass-loader'],
			},
		],
	},
	plugins: [
		new ExtractTextPlugin({
			filename: '[name].css',
			allChunks: true,
		}),
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery"
		})
	],
};
